from django.conf import settings


def cookiebot_js(request):
    return {
        "cookiebot_js_uc": '<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="'
        + settings.COOKIEBOT_ID
        + '" data-blockingmode="auto" type="text/javascript"></script>',
        "cookiebot_js_cd": '<script id="CookieDeclaration" src="https://consent.cookiebot.com/'
        + settings.COOKIEBOT_ID
        + '/cd.js" type="text/javascript" async></script>',
    }
